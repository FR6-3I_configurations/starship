<div align='center'>

  # Starship

</div>

## 📦 Dependencies (Arch)

### ❗ Required


Official :

```shell
paru -S starship
```

## ⚙️ Configuration

Symbolic link :

```shell
ln -s ~/.config/starship/starship.toml ~/.config/starship.toml
```
